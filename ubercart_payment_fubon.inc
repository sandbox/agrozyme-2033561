<?php

module_load_include('module', 'ubercart_payment_gateway');

abstract class UbercartPaymentFubonController extends UbercartPaymentGatewayController {

	function __construct() {
		parent::__construct();
		$default = &$this->controller_data_defaults;
		$default['payment_currency'] = 'TWD';
		$default['payment_decimal'] = 0;
		$default['display_decimal'] = 0;
	}

}

abstract class UbercartPaymentFubonCore extends UbercartPaymentGatewayCore {

	function paymentCurrencyOption() {
		$index = 'TWD';
		$list = parent::paymentCurrencyOption();
		return array($index => $list[$index]);
	}

}

class UbercartPaymentFubonCreditcardCore extends UbercartPaymentFubonCore {

	function initMethod() {
		$data = parent::initMethod();
		$data['name'] = t('Fubon Credit Card');
		$data['title'] = t('Fubon Credit Card');
		return $data;
	}

	function initAction() {
		$data = parent::initAction();
		unset($data['query']);
		$payment = &$data['payment'];
		$payment['returnOrderNumberIndex'] = 'ORDERID';
		$payment['returnMessageIndex'] = 'RESPONSEMSG';
		$payment['returnCodeIndex'] = 'RESPONSECODE';
		$payment['returnSuccessStatusList'] = array('000' => 'payment_received');
		$payment['returnOtherStatus'] = 'Canceled';
		$payment['returnKeyList'] = array(
			'MERCHANTID' => 'MERCHANTID',
			'TERMINALID' => 'TERMINALID',
			'TRANSDATE',
			'TRANSTIME',
			'RESPONSECODE',
			'RESPONSEMSG',
			'APPROVECODE',
			'ORDERID' => 'ORDERID',
			'SYSORDERID',
			'BATCHNO',
			'TRANSTYPE',
			'CURRENCYCODE',
			'TRANSCODE',
			'TRANSAMT',
			'TRANSMODE' => 'TRANSMODE',
		);
		return $data;
	}

	function installmentOption() {
		$data = array(1, 3, 6, 12);
		return array_combine($data, $data);
	}

	function schemaBody() {
		$data = parent::schemaBody();
		$field = &$data['fields'];
		$field['merchant_code']['length'] = 15;
		$field['counter_code'] = array(
			'type' => 'varchar',
			'length' => 8,
			'not null' => TRUE,
			'default' => '',
		);

		unset($field['merchant_key']);
		return $data;
	}

	function methodSetting(array $form, array &$form_state) {
		$form = parent::methodSetting($form, $form_state);
		unset($form['currency']);
		return $form;
	}

	function methodSettingBasicFieldset(array $data) {
		$default = $data['default'];
		$field = $data['field'];
		$list = parent::methodSettingBasicFieldset($data);
		$list['counter_code'] = array(
			'#type' => 'textfield',
			'#title' => t('Counter Code'),
			'#default_value' => $default['counter_code'],
			'#maxlength' => $field['counter_code']['length'],
			'#required' => TRUE,
		);
		unset($list['merchant_key']);
		return $list;
	}

	function paymentSetting(array $form, array &$form_state) {
		$form = parent::paymentSetting($form, $form_state);
		unset($form['setting']);
		return $form;
	}

	function paymentSettingSettingFieldset(array $data) {

		$default = &$data['default'];
		$default += array('installment' => 1);

		$form = parent::paymentSettingSettingFieldset($data);
		$form['installment'] = array(
			'#type' => 'select',
			'#title' => t('Installment'),
			'#options' => $this->installmentOption(),
			'#default_value' => $default['installment'],
			'#required' => TRUE,
		);

		return $form;
	}

}

